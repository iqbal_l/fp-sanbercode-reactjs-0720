import React, {useContext} from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import {MovieContext} from '../../../context/MovieContext'

import BlogMovie from './blogmovies'

export default function Section(props) {
  const [movies] = useContext(MovieContext)
  const movie_10 = movies.slice(0, 10);


  const useStyles = makeStyles((theme) => ({
    heroContent: {
      padding: theme.spacing(2, 0, 2),
      backgroundColor: '#F5F5F5'
    }
  }));

  const classes = useStyles();

  return (
    <React.Fragment>
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          {movie_10.map((item, index) => (
              <BlogMovie key={index} movies={item} />
            ))
          }
        </Grid>
      </Container>
    </React.Fragment>
  );
}
