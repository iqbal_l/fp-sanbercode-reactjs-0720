import React, {useState, useEffect} from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Alert } from '@material-ui/lab';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';


import {Link as LinkRoute, useHistory, useParams} from 'react-router-dom'
import axios from 'axios';

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(1),
  },
}));

export default function Form() {
  const classes = useStyles();
  const kosong = {name:"",genre:"",image_url:"",release:"",platform:""}
  const [games, setGames] = useState([])
  const [input, setInput] = useState(kosong)
  const [error, setError] = useState(null)
  const { id } = useParams();
  const [statusForm, setStatusForm]  =  useState((typeof id !== 'undefined')?'update':'create')

  if (typeof id !== 'undefined'){

  }

  const history = useHistory();

  useEffect( () => {
    if (games.length === 0){
      if (typeof id !== 'undefined'){
        axios.get(`https://backendexample.sanbersy.com/api/games/${id}`)
          .then(res => {
            console.log(res)
            setInput(res.data)
            setGames(res.data)
            })
      }
    }
   })

  const handleChange = (event) =>{
    let type = event.target.name
    setInput({
      ...input, [event.target.name] : ((type === 'multiplayer' || type === 'singlePlayer')?((event.target.checked === true)?1:0):((event.target.name === 'release')?event.target.value.replace(/[^0-9]/g, ''):event.target.value))
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    if (input.name.replace(/\s/g,'') !== '' && input.genre.replace(/\s/g,'') !== '' && input.release.replace(/\s/g,'') !== '' && input.image_url.replace(/\s/g,'') !== '' ){
        if (statusForm === 'create'){
          axios.post(`https://backendexample.sanbersy.com/api/games`,input)
          .then(res => {
          })
        }else{
          axios.put(`https://backendexample.sanbersy.com/api/games/${id}`,input)
          .then(res => {
          })
        }

        setError({message:'Data berhasil disimpan',type:'success'})
    }else{
        setError({message:'Data gagal disimpan',type:'error'})
    }
    setInput(kosong)
    setGames([])
  }

  return (
    <React.Fragment>
      { error !== null ?
          <Alert severity={error.type}>{error.message}</Alert> :""
    }
    <Grid item xs={12}>
      <form method="POST" onSubmit={(event)=>{handleSubmit(event)}}>

      <TextField
        variant="outlined"
        required
        fullWidth
        label="Judul Game"
        name="name"
        className={classes.seeMore}
        value={input.name}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Genre"
        name="genre"
        className={classes.seeMore}
        value={input.genre}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Release"
        name="release"
        className={classes.seeMore}
        value={input.release}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Url Image"
        name="image_url"
        className={classes.seeMore}
        value={input.image_url}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="platform"
        name="platform"
        className={classes.seeMore}
        value={input.platform}
        onChange={(e) => {handleChange(e)}}
      />
      <FormControlLabel
          control={
            <Checkbox
              checked={(input.multiplayer === 1)?true:false}
              name="multiplayer"
              color="primary"
              onChange={(e) => {handleChange(e)}}
            />
          }
          label="Multiplayer"
        />
        <FormControlLabel
            control={
              <Checkbox
                checked={(input.singlePlayer === 1)?true:false}
                name="singlePlayer"
                color="primary"
                onChange={(e) => {handleChange(e)}}
              />
            }
            label="Single Player"
          />
      <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
            className={classes.seeMore}
        >
          Simpan
        </Button>
      </form>
      </Grid>
    </React.Fragment>
  );
}
