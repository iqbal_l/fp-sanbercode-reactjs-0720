import React, {useState, useEffect} from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Alert } from '@material-ui/lab';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';


import {Link as LinkRoute, useHistory, useParams} from 'react-router-dom'
import axios from 'axios';

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(1),
  },
}));

export default function Form() {
  const classes = useStyles();
  const kosong = {title:"",genre:"",image_url:"",year:"",description:"",review:"",rating:""}
  const [movies, setMovies] = useState([])
  const [input, setInput] = useState(kosong)
  const [error, setError] = useState(null)
  const { id } = useParams();
  const [statusForm, setStatusForm]  =  useState((typeof id !== 'undefined')?'update':'create')

  if (typeof id !== 'undefined'){

  }

  const history = useHistory();

  useEffect( () => {
    if (movies.length === 0){
      if (typeof id !== 'undefined'){
        axios.get(`https://backendexample.sanbersy.com/api/movies/${id}`)
          .then(res => {
            setInput(res.data)
            setMovies(res.data)
            })
      }
    }
   })

  const handleChange = (event) =>{
    let type = event.target.name
    setInput({
      ...input, [event.target.name] : ((event.target.name === 'year' || event.target.name === 'review' || event.target.name === 'rating')?event.target.value.replace(/[^0-9]/g, ''):event.target.value)
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    if (input.genre.replace(/\s/g,'') !== '' && input.title.replace(/\s/g,'') !== '' ){
        if (statusForm === 'create'){
          axios.post(`https://backendexample.sanbersy.com/api/movies`,input)
          .then(res => {
          })
        }else{
          axios.put(`https://backendexample.sanbersy.com/api/movies/${id}`,input)
          .then(res => {
          })
        }

        setError({message:'Data berhasil disimpan',type:'success'})
    }else{
        setError({message:'Data gagal disimpan',type:'error'})
    }
    setInput(kosong)
    setMovies([])
  }

  return (
    <React.Fragment>
      { error !== null ?
          <Alert severity={error.type}>{error.message}</Alert> :""
    }
    <Grid item xs={12}>
      <form method="POST" onSubmit={(event)=>{handleSubmit(event)}}>

      <TextField
        variant="outlined"
        required
        fullWidth
        label="Judul Movies"
        name="title"
        className={classes.seeMore}
        value={input.title}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Genre"
        name="genre"
        className={classes.seeMore}
        value={input.genre}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Year"
        name="year"
        className={classes.seeMore}
        value={input.year}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Url Image"
        name="image_url"
        className={classes.seeMore}
        value={input.image_url}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Rating"
        name="rating"
        className={classes.seeMore}
        value={input.rating}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Review"
        name="review"
        className={classes.seeMore}
        value={input.review}
        onChange={(e) => {handleChange(e)}}
      />
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Description"
        name="description"
        rows={4}
        multiline
        className={classes.seeMore}
        value={input.description}
        onChange={(e) => {handleChange(e)}}
      />



      <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
            className={classes.seeMore}
        >
          Simpan
        </Button>
      </form>
      </Grid>
    </React.Fragment>
  );
}
