import React, {useContext} from "react"
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Home from "../pages/frontend/home/index"

import Movies from "../pages/frontend/movies/index"
import MoviesDetail from "../pages/frontend/movies/detail"

import Games from "../pages/frontend/games/index"
import GamesDetail from "../pages/frontend/games/detail"

import Login from "../pages/frontend/login/index"
import Register from "../pages/frontend/register/index"

import MoviesAdmin from "../pages/backend/movies/index"
import MoviesAdminCreate from "../pages/backend/movies/tambah"
import MoviesAdminUbah from "../pages/backend/movies/ubah"

import GamesAdmin from "../pages/backend/games/index"
import GamesAdminCreate from "../pages/backend/games/tambah"
import GamesAdminUbah from "../pages/backend/games/ubah"

import {UserContext} from "../context/UserContext"


const Section = () =>{

  const [user] = useContext(UserContext);

  const PrivateRoute = ({user, ...props }) => {
    if (user) {
      return <Route {...props} />;
    } else {
      return <Redirect to="/login" />;
    }
  };

  const LoginRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;

  return(
    <section >
      <Switch>
        <Route exact path="/" user={user} component={Home}/>
        <Route exact path="/games" user={user} component={Games}/>
        <Route exact path="/games/:id" user={user} component={GamesDetail}/>
        <Route exact path="/movies" user={user} component={Movies}/>
        <Route exact path="/movies/:id" user={user} component={MoviesDetail}/>
        <Route exact path="/register" user={user} component={Register}/>
        <LoginRoute exact path="/login" user={user} component={Login}/>

        <PrivateRoute exact path="/admin/games" user={user} component={GamesAdmin}/>
        <PrivateRoute exact path="/admin/games/create" user={user} component={GamesAdminCreate}/>
        <PrivateRoute exact path="/admin/games/ubah/:id" user={user} component={GamesAdminUbah}/>

        <PrivateRoute exact path="/admin/movies" user={user} component={MoviesAdmin}/>
        <PrivateRoute exact path="/admin/movies/create" user={user} component={MoviesAdminCreate}/>
        <PrivateRoute exact path="/admin/movies/ubah/:id" user={user} component={MoviesAdminUbah}/>
      </Switch>
    </section>
  )
}

export default Section
