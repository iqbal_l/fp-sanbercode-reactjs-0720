import React, {useContext} from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import {MovieContext} from '../../../context/MovieContext'
import {GameContext} from '../../../context/GameContext'

import BlogMovies from './blogmovies'
import BlogGames from './bloggames'

export default function Section(props) {
  const [movies] = useContext(MovieContext)
  const movies_4 = movies.slice(0, 4);

  const [games] = useContext(GameContext)
  const games_4 = games.slice(0, 4);


  const useStyles = makeStyles((theme) => ({
    heroContent: {
      padding: theme.spacing(2, 0, 2),
      backgroundColor: '#F5F5F5'
    }
  }));

  const classes = useStyles();

  return (
    <React.Fragment>
      <Container maxWidth="lg">
        <Container maxWidth="sm" component="main" className={classes.heroContent}>
          <Typography component="h4" variant="h4" align="center" color="textPrimary" gutterBottom>
            === Daftar List Movies ===
          </Typography>
        </Container>
        <Grid container spacing={2}>
          {movies_4.map((item, index) => (
              <BlogMovies key={index} movies={item} />
            ))
          }
        </Grid>
      </Container>
      <Container maxWidth="lg">
        <Container maxWidth="sm" component="main" className={classes.heroContent}>
          <Typography component="h4" variant="h4" align="center" color="textPrimary" gutterBottom>
            === Daftar List Games ===
          </Typography>
        </Container>
        <Grid container spacing={2}>
          {games_4.map((item, index) => (
              <BlogGames key={index} games={item} />
            ))
          }
        </Grid>
      </Container>
    </React.Fragment>
  );
}
