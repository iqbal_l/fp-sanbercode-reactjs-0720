import React, {useContext, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import {UserContext} from '../../../context/UserContext'
import axios from 'axios'

import {
  Link, useHistory
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  link: {
    textDecorationStyle: 'none'
  },
  errorShow: {
    color:'red'
  },

}));

const Register = () => {
  const classes = useStyles();
  const [, setUser] = useContext(UserContext);
  const [input, setInput] = useState({username:"",password:""})
  const [error, setError] = useState("")

  const history = useHistory();


  const handleChange = (event) =>{
    let name = event.target.name
    let value = event.target.value

    setInput({
      ...input, [name] : value
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    if (input.username.replace(/\s/g,'') !== '' && input.password.replace(/\s/g,'') !== ''){
        axios.post(`https://backendexample.sanbersy.com/api/users`,input)
        .then(res => {
          setUser(res)
          localStorage.setItem("user", JSON.stringify(res))
        })

        history.push('/login')
    }else{
      setError("Username atau Password anda belum diisi")
    }


  }

  return (
    <>
    {console.log(error)}
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={(e) => {handleSubmit(e)}} method="POST">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address/Username"
                name="username"
                autoComplete="email"
                onChange={(e) => {handleChange(e)}}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) => {handleChange(e)}}
              />
            </Grid>
          </Grid>
          <Grid container justify="flex-end" className={classes.errorShow}>
            {error}
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>

          <Grid container justify="flex-end">
            <Grid item>
              <Link to='/login' variant="body2" className={classes.link}>
              Sudah punya akun ? silahkan log in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>

    </Container>
    </>
  );
}

export default Register
