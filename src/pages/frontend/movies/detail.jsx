import React, {useEffect, useState} from 'react';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';

import {MovieProvider} from '../../../context/MovieContext'
import {useParams} from 'react-router-dom'
import axios from 'axios'

const App = ({}) => {
  const [movies, setMovies] = useState([])
  const { id } = useParams();

  const useStyles = makeStyles((theme) => ({
    heroContent: {
      padding: theme.spacing(8, 0, 6),
    },
      card: {
        display: 'flex',
      },
      cardDetails: {
        flex: 1,
      },
      cardMedia: {
        width: 450,
        height:350
      },
  }));

  const classes = useStyles();
  useEffect( () => {
    if (movies.length === 0){
      if (typeof id !== 'undefined'){
        axios.get(`https://backendexample.sanbersy.com/api/movies/${id}`)
          .then(res => {
            setMovies(res.data)
            })
      }
    }
   })

  return (
      <React.Fragment>
        <MovieProvider>
          <Container maxWidth="lg" component="main" className={classes.heroContent}>
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              {movies.name}
            </Typography>
            <Grid item xs={12} md={12}>
              <CardActionArea component="a" href="#">
                <Card className={classes.card}>
                  <div className={classes.cardDetails}>
                    <CardContent>

                      <Typography variant="subtitle1" paragraph>
                          Review : {movies.review}
                      </Typography>
                      <Typography variant="subtitle1" paragraph>
                          Rating : {movies.rating}
                      </Typography>
                      <Typography variant="subtitle1" paragraph>
                          Genre : {movies.genre}
                      </Typography>
                      <Typography variant="subtitle1" paragraph>
                        Tahun : {movies.year}
                      </Typography>
                      <Typography variant="subtitle1" paragraph>
                        Synopsis : {movies.description}
                      </Typography>
                    </CardContent>
                  </div>
                  <Hidden xsDown>
                    <CardMedia className={classes.cardMedia} image={movies.image_url} title={movies.title} />
                  </Hidden>
                </Card>
              </CardActionArea>
            </Grid>
          </Container>
        </MovieProvider>
      </React.Fragment>
  );
};

export default App;
