import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
//layouts
import TopBar from './topbar'
import Section from './section'
import Footer from './footer'

import { useLocation } from 'react-router-dom';



export default function Main() {
  const location = useLocation();

  return (
    <React.Fragment>
      <CssBaseline />
      { (location.pathname === '/login' || location.pathname === '/register')?"":
          <TopBar/>
        }

        <Section/>
          { (location.pathname === '/login' || location.pathname === '/register')?"":
                <Footer/>
            }

    </React.Fragment>
  );
}
