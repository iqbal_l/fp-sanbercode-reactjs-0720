import React,{useContext} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';

import { Link, useHistory } from "react-router-dom";
import { UserContext } from "../context/UserContext";


const useStyles = makeStyles((theme) => ({
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: 'wrap',
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
    textDecorationStyle: 'none'
  },
}));

export default function TopBar() {
  const classes = useStyles();
  const [user, setUser] = useContext(UserContext)
  const history = useHistory()
  const handleLogout = () => {
    setUser(null)
    localStorage.setItem("user", null)

    history.push('/')
  }

  console.log(user)
  return (
    <>
    <AppBar position="sticky" color="default" elevation={0} className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
          Movie & Game Catalog
        </Typography>
          <Link variant="button" color="textPrimary" to="/" className={classes.link}>
            Beranda
          </Link>
          <Link variant="button" color="textPrimary" to="/games" className={classes.link}>
            Games
          </Link>
          <Link variant="button" color="textPrimary" to="/movies" className={classes.link}>
            Movies
          </Link>
          { (user === null)?
              <Link variant="button" color="textPrimary" to="/login" className={classes.link}>
                <Button  color="primary" variant="outlined" className={classes.link}>
                  Login
                </Button>
              </Link>
              :
              <div>
              <Link variant="button" color="textPrimary" to="/admin/movies" className={classes.link}>
                <Button  color="danger" variant="outlined" className={classes.link}>
                  Admin
                </Button>
              </Link>
              <Link onClick={()=>handleLogout()} className={classes.link}>
                  Logout
              </Link>
            </div>
          }
      </Toolbar>
    </AppBar>
    </>
  );
}
