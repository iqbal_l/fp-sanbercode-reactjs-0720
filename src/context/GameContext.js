import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const GameContext = createContext();

export const GameProvider = props => {
  const [games, setGames] = useState([])


  useEffect( () => {
    if (games.length === 0){
       axios.get(`https://backendexample.sanbersy.com/api/games`)
       .then(res => {
         setGames(res.data.map(el => {return {
           id:el.id,
           year:el.year,
           name:el.name,
           platform:el.platform,
           release:el.release,
           genre:el.genre,
           multiplayer:el.multiplayer,
           single:el.single,
           image_url:el.image_url
         }}))
       })
     }
   })



    return (
      <GameContext.Provider value={[games, setGames]}>
        {props.children}
      </GameContext.Provider>
    );
};

export default GameProvider
