import React, {useContext} from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import {GameContext} from '../../../context/GameContext'

import BlogGames from './bloggames'

export default function Section(props) {
  const [games] = useContext(GameContext)
  const games_10 = games.slice(0, 10);


  const useStyles = makeStyles((theme) => ({
    heroContent: {
      padding: theme.spacing(2, 0, 2),
      backgroundColor: '#F5F5F5'
    }
  }));

  const classes = useStyles();

  return (
    <React.Fragment>
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          {games_10.map((item, index) => (
              <BlogGames key={index} games={item} />
            ))
          }
        </Grid>
      </Container>
    </React.Fragment>
  );
}
