import React from 'react';

import Header from './header'
import Section from './section'

import {GameProvider} from '../../../context/GameContext'

const App = ({}) => {
  return (
      <React.Fragment>
        <GameProvider>
          <Header />
          <Section />
        </GameProvider>
      </React.Fragment>
  );
};

export default App;
