import React from 'react';
import logo from './logo.svg';
import './App.css';
import {useLocation } from "react-router-dom";
import Main from './layouts/main';
import MainBackend from './layouts/backend/main';

const App = () => {
  const location = useLocation();
  const str = location.pathname;
  const isadmin = str.includes('/admin')


  return (
    <>
        {

          (isadmin === false)?
              <Main />
              :
              <MainBackend />
        }

    </>
  );
}


export default App;
