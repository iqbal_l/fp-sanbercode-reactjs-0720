import React from 'react';

import Header from './header'
import Section from './section'

import {MovieProvider} from '../../../context/MovieContext'
import {GameProvider} from '../../../context/GameContext'

const App = ({}) => {
  return (
      <React.Fragment>
        <GameProvider>
          <MovieProvider>
            <Header />
            <Section />
          </MovieProvider>
        </GameProvider>
      </React.Fragment>
  );
};

export default App;
