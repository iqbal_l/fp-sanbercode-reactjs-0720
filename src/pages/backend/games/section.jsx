import React, {useContext, useState, useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';

import {GameContext, } from '../../../context/GameContext'
import axios from 'axios'
import {Link as LinkRoute} from 'react-router-dom'

export default function Section(props) {
  const [games, setGames] = useContext(GameContext)

  const [input, setInput] = useState([])
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")
  const [temp, setTemp] =useState([])
  const games_10 = temp.slice(0, 10)
  let name;
  let platform;
  let genre;

  const useStyles = makeStyles((theme) => ({
    heroContent: {
      padding: theme.spacing(2, 0, 2),
      backgroundColor: '#F5F5F5'
    }
  }));

  const classes = useStyles();

  useEffect( () => {
    if (temp.length === 0){
       setTemp(games)
     }
   })

  const handleCari = (event) => {
    const newGames = [{id:'huruf'}];
    let value = event.target.value.toLowerCase()
    let i = 0;

    setTimeout(() =>{
      games !== null && games.find((item) => {
        if (item.name.toLowerCase().includes(value) == true || item.platform.toLowerCase().includes(value) == true || item.genre.toLowerCase().includes(value) == true){
          newGames[i] = item
          i++;
        }
      })


      setTemp(newGames)
    },300)
    if (value === ''){
      setTemp([])
    }
  }

  const handleChange = (event) => {
    let name = event.target.name
    let value = event.target.value

    setInput({
      name : value
    })
  }

  const handleHapus = (id_games) => {
    let id = parseInt(id_games)
    let newDaftar = games.filter(el => el.id != id)
    console.log(id)
    axios.delete(`https://backendexample.sanbersy.com/api/games/${id}`)
    .then(res => {
    })

    setGames([...newDaftar])
    setTemp([...newDaftar])

  }

  const handleEdit  = (id_games) => {
    let id = parseInt(id_games)
    let load = games.find(x=> x.id === id)
    setInput(load)
    setSelectedId(id)
    setStatusForm("edit")
  }

  return (
    <React.Fragment>
      <Grid item xs={12} md={6}>
      <TextField
        variant="outlined"
        required
        fullWidth
        label="Cari Game"
        name="cari"
        onChange={(e) => {handleCari(e)}}
      />
    </Grid>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>No</TableCell>
            <TableCell>Game</TableCell>
            <TableCell>Release</TableCell>
            <TableCell>Genre</TableCell>
            <TableCell>Platform</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {games_10 !== null && games_10.map((rows, index) =>  (
            <>
            { (isNaN(rows.id)===false)?
              <TableRow key={rows.id}>
                <TableCell>{index+1}</TableCell>
                <TableCell>{rows.name}</TableCell>
                <TableCell>{rows.release}</TableCell>
                <TableCell>{rows.genre}</TableCell>
                <TableCell>{rows.platform}</TableCell>
                <TableCell>
                  <LinkRoute to={"/admin/games/ubah/"+rows.id}>
                  <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    style={{fontSize:10,padding:2,margin:2}}
                    value={rows.id}
                    onClick ={(event)=>handleEdit(rows.id)}
                  >
                    Edit
                  </Button>
                </LinkRoute>
                  <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="secondary"
                    style={{fontSize:10,padding:2,margin:2}}
                    value={rows.id}
                    onClick ={(event)=>handleHapus(rows.id)}
                  >
                    Hapus
                  </Button>
                </TableCell>
              </TableRow>
              : ""
            }
            </>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
}
