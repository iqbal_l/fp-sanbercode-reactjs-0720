import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Container from '@material-ui/core/Container';
import Hidden from '@material-ui/core/Hidden';

import {Link as LinkRoute} from 'react-router-dom'

const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 180,
  },
});

export default function Blog({movies, ...props}) {
  const classes = useStyles();

  return (
    <>
      <Grid item xs={12} md={12}>
        <LinkRoute to={'/movies/'+movies.id}>
        <CardActionArea component="a" href="#">
          <Card className={classes.card}>
            <div className={classes.cardDetails}>
              <CardContent>
                <Typography component="h2" variant="h5">
                  {movies.title}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  Rating - {movies.rating}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  Tahun Keluaran - {movies.year}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  {movies.genre}
                </Typography>

                { /*<Typography variant="subtitle1" color="primary">*/ }
                { /*  Continue reading...*/ }
                { /*</Typography>*/ }
              </CardContent>
            </div>
            <Hidden xsDown>
              <CardMedia className={classes.cardMedia} image={movies.image_url} title={movies.title} />
            </Hidden>
          </Card>
        </CardActionArea>
      </LinkRoute>
      </Grid>
    </>
  );
}
