import React, {useContext} from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Button from '@material-ui/core/Button';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import Section from './section'
import {MovieProvider} from '../../../context/MovieContext'

import {Link as LinkRoute} from 'react-router-dom'

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Orders() {
  const classes = useStyles();


  return (
    <React.Fragment>
      <MovieProvider>
        <LinkRoute to='/admin/movies/create' >
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
          >
            Tambah Movies
          </Button>
        </LinkRoute>
      <Typography component="h2" variant="h6" color="primary" gutterBottom>
      Pengaturan Movies
      </Typography>

      <Section />
    </MovieProvider>
    </React.Fragment>
  );
}
