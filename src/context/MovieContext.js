import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const MovieContext = createContext();

export const MovieProvider = props => {
  const [movies, setMovies] = useState([])

  useEffect( () => {
    if (movies.length === 0){
       axios.get(`https://backendexample.sanbersy.com/api/movies`)
       .then(res => {
         setMovies(res.data.map(el => {return {
           id:el.id,
           year:el.year,
           rating:el.rating,
           review:el.review,
           rating:el.rating,
           genre:el.genre,
           description:el.description,
           title:el.title,
           image_url:el.image_url
         }}))
       })
     }
   })

    return (
      <MovieContext.Provider value={[movies,setMovies]}>
        {props.children}
      </MovieContext.Provider>
    );
};

export default MovieProvider
