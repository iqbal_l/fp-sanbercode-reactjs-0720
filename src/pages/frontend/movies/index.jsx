import React from 'react';

import Header from './header'
import Section from './section'

import {MovieProvider} from '../../../context/MovieContext'

const App = ({}) => {
  return (
      <React.Fragment>
        <MovieProvider>
          <Header />
          <Section />
        </MovieProvider>
      </React.Fragment>
  );
};

export default App;
