import React, {useEffect, useState} from 'react';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';

import {GameProvider} from '../../../context/GameContext'
import {useParams} from 'react-router-dom'
import axios from 'axios'

const App = ({}) => {
  const [games, setGames] = useState([])
  const { id } = useParams();

  const useStyles = makeStyles((theme) => ({
    heroContent: {
      padding: theme.spacing(8, 0, 6),
    },
      card: {
        display: 'flex',
      },
      cardDetails: {
        flex: 1,
      },
      cardMedia: {
        width: 450,
        height:350
      },
  }));

  const classes = useStyles();
  useEffect( () => {
    if (games.length === 0){
      if (typeof id !== 'undefined'){
        axios.get(`https://backendexample.sanbersy.com/api/games/${id}`)
          .then(res => {
            setGames(res.data)
            })
      }
    }
   })

  return (
      <React.Fragment>
        <GameProvider>
          <Container maxWidth="lg" component="main" className={classes.heroContent}>
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              {games.name}
            </Typography>
            <Grid item xs={12} md={12}>
              <CardActionArea component="a" href="#">
                <Card className={classes.card}>
                  <div className={classes.cardDetails}>
                    <CardContent>

                      <Typography variant="subtitle1" paragraph>
                          Platform : {games.platform} - {(games.multiplayer === 1)?'Multiplayer':'Single Player'}
                      </Typography>
                      <Typography variant="subtitle1" paragraph>
                          Genre : {games.genre}
                      </Typography>
                      <Typography variant="subtitle1" paragraph>
                        Tahun Release : {games.release}
                      </Typography>
                    </CardContent>
                  </div>
                  <Hidden xsDown>
                    <CardMedia className={classes.cardMedia} image={games.image_url} title={games.name} />
                  </Hidden>
                </Card>
              </CardActionArea>
            </Grid>
          </Container>
        </GameProvider>
      </React.Fragment>
  );
};

export default App;
